
// Pega valor de elemento pelo id
function getValue( id ) {
    if ( typeof(document.getElementById) != 'undefined' )
        return document.getElementById( id );
}

// Verifica se CPF é válido
function isValidCPF(strCPF) {
    var Soma;
    var Resto;
    Soma = 0;
    if (strcpf ==== "00000000000")
        return false;
    for (i=1; i<=9; i++)
        Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;
    if ((Resto === 10) || (Resto === 11))
        Resto = 0;
    if (Resto !== parseInt(strCPF.substring(9, 10)) )
        return false;
    Soma = 0;
    for (i = 1; i <= 10; i++)
        Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;
    if ((Resto === 10) || (Resto === 11))
        Resto = 0;
    if (Resto !== parseInt(strCPF.substring(10, 11) ) )
        return false;
    return true;
}

function formatCPF(xElement) {

    let strValor = getValue(xElement).value;
    let strTemp;

    strTemp = strValor.replace(".", "");
    strTemp = strTemp.replace(".", "");
    strTemp = strTemp.replace(".", "");
    strTemp = strTemp.replace("-", "");
    strTemp = strTemp.replace("-", "");

    strValor = strTemp

    if (strValor.length > 9) {
        strValor = strValor.substr(0, 3) + '.' + strValor.substr(3, 3) + '.' + strValor.substr(6, 3) + '-' + strValor.substr(9, 2);
    }
    else if (strValor.length > 6) {
        strValor = strValor.substr(0, 3) + '.' + strValor.substr(3, 3) + '.' + strValor.substr(6, 3);
    }
    else if (strValor.length > 3) {
        strValor = strValor.substr(0, 3) + '.' + strValor.substr(3, 3);
    }

    getValue(xElement).value = strValor;
}

/* VALIDAR CPF *****************
Na linha 2, removemos todos os caracteres não númericos do CPF passado como parâmetro,
eliminando uma possível máscara.

O condicional if da linha 5 verifica se número de dígitos da string já limpa é igual a 11 e checa por valores iguais.
Esta verificação é necessária uma vez que se aplicarmos o algoritmo do CPF sobre o número "111.111.111-11" teoricamente
os dígitos verificadores estão corretos, mas este NÃO é um número válido.

As linha 17 à 25 verificam se o primeiro dígito verificador é válido de acordo com o algoritmo do CPF.
Caso negativo, a validação já retorna false encerrando a função.

Já as linha 26 à 34 verificam se o segundo dígito verificador é válido.

Caso o algoritmo alcance a linha 35 temos um CPF válido e o boolean true é retornado.
* */
function validarCPF(cpf) {
    cpf = cpf.replace(/[^\d]+/g,'');
    if(cpf === '') return false;
    // Elimina CPFs invalidos conhecidos	
    if (cpf.length !== 11 ||
        cpf === "00000000000" ||
        cpf === "11111111111" ||
        cpf === "22222222222" ||
        cpf === "33333333333" ||
        cpf === "44444444444" ||
        cpf === "55555555555" ||
        cpf === "66666666666" ||
        cpf === "77777777777" ||
        cpf === "88888888888" ||
        cpf === "99999999999")
        return false;
    // Valida 1o digito	
    add = 0;
    for (i=0; i < 9; i ++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
    rev = 11 - (add % 11);
    if (rev === 10 || rev === 11)
        rev = 0;
    if (rev !== parseInt(cpf.charAt(9)))
        return false;
    // Valida 2o digito	
    add = 0;
    for (i = 0; i < 10; i ++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev === 10 || rev === 11)
        rev = 0;
    if (rev !== parseInt(cpf.charAt(10)))
        return false;
    return true;
}

function isValidCpf (cpf) {
    // Validar se é String
    if (typeof cpf !== 'string') return false

    // Tirar formatação
    cpf = cpf.replace(/[^\d]+/g, '')

    // Validar se tem tamanho 11 ou se é uma sequência de digitos repetidos
    if (cpf.length !== 11 || !!cpf.match(/(\d)\1{10}/)) return false

    // String para Array
    cpf = cpf.split('')

    const validator = cpf
        // Pegar os últimos 2 digitos de validação
        .filter((digit, index, array) => index >= array.length - 2 && digit)
        // Transformar digitos em números
        .map( el => +el )

    const toValidate = pop => cpf
        // Pegar Array de items para validar
        .filter((digit, index, array) => index < array.length - pop && digit)
        // Transformar digitos em números
        .map(el => +el)

    const rest = (count, pop) => (toValidate(pop)
            // Calcular Soma dos digitos e multiplicar por 10
            .reduce((soma, el, i) => soma + el * (count - i), 0) * 10)
        // Pegar o resto por 11
        % 11
        // transformar de 10 para 0
        % 10

    return !(rest(10,2) !== validator[0] || rest(11,1) !== validator[1])
}

/* Retorna apenas números */
function onlyNumber(string)
{
    const number = string.replace(/[^0-9]/g,'');
    return parseInt(number);
}

function onlyNumberAndLetter(string) {
    return string.replace(/[^a-zA-Z0-9]/g, '');
}

function onlyLetter(string) {
    return string.replace(/[^a-zA-Z]/g, '');
}